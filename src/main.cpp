#include "HardwareSerial.h"
#include "BLEDevice.h"
#include "BLEServer.h"
#include "BLEUtils.h"
#include "Arduino.h"
#include "modules/EEPROMUtil.hpp"
#include "modules/bluetooth_works.hpp"
#include "modules/battery_works.hpp"
#include "modules/buzzer_works.hpp"
#include "modules/l298n_works.hpp"
#include "modules/led_lights_works.hpp"
#include "modules/nrf24l01_works.hpp"
#include "modules/print_screen_works.hpp"
#include "modules/servo_works_esp32.hpp"
#include "modules/settings_n_structs.hpp"
#include "modules/watch_dog_esp32.hpp"
#include "modules/trimming_works.hpp"
#include "modules/touch_sensor.hpp"
#include "modules/push_button.hpp"
#include <time.h>

using namespace std;


Servo ServoOutputA;  // create servo object to control a servo
SYSTEM_SETTINGS_STRUCT system_settings;


BLEServer *pServer;
BLECharacteristic *pCharacteristic;
BLEUUID mUUID;
BLEAdvertising *pAdvertising;
BLEService *pService ;

bool bleDeviceConnected = false;
bool oldBLEDeviceConnected = false;
int memVar = 0 ;

void printHeapStatus() {
    int tmpVar = ESP.getFreeHeap() ;
    if ( memVar != tmpVar ){
        Serial.print("Free heap memory: ");
        Serial.println(tmpVar);
        memVar = tmpVar;
    }
}

void setup() {
    Serial.begin(115200) ;

    EEPROMUtil::begin(&system_settings);
    push_button_initial( &system_settings );
    touch_sensor_initial( &system_settings );
    bluetooth_initialization( &system_settings );
    battery_works_initi( &system_settings );
    passive_buzzer_initial( &system_settings );
    nrf24l01_initial( &system_settings );
    servoESP32_initial( &system_settings );
    l298n_initial( &system_settings );
    led_lights_initi( &system_settings );
    watch_dog_initial( &system_settings );
    system_settings.led_fading_last_update = millis();
}

void loop() {
    // printHeapStatus();
    push_button_routine( &system_settings );
    touch_sensor_routine( &system_settings );
    BluetoothMainProcess( &system_settings );
    nrf24l01_routine( &system_settings );
    trimming_works_routine( &system_settings );
    horn_routine(&system_settings);
    servoESP32_routine( &system_settings );
    l298n_routine( &system_settings );
    led_lights_routine( &system_settings );
    battery_works_routine( &system_settings );
    watch_dog_routine( &system_settings );
    print_console_routine( &system_settings );
}
