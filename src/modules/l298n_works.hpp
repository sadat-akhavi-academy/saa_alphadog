#ifndef L298N_WORKS_INITIAL_HPP
#define L298N_WORKS_INITIAL_HPP

#include "Arduino.h"
#include "string.h"
#include "modules/settings_n_structs.hpp"

#define enA 33 
#define in1 25
#define in2 26
#define in3 27
#define in4 14
#define enB 12

int motorSpeedA = 0;
int motorSpeedB = 0;

int customMap(int x, int in_min, int in_max, int out_min, int out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


int softStartStop(SYSTEM_SETTINGS_STRUCT *system_settings, int targetSpeed, int K, int J) {
    unsigned long now = millis();
    int currentSpeed = system_settings->currentSpeed;

    // Calculate step size based on softness factor (K for start, J for stop)
    int stepSize = (targetSpeed > currentSpeed) ? (255 - K) / 10 : (255 - J) / 10;
    if (stepSize < 1) stepSize = 1; // Ensure a minimum step size

    // Limit updates to every 10ms for non-blocking behavior
    if (now - system_settings->lastUpdate < 10) {
        return currentSpeed;
    }

    system_settings->lastUpdate = now;

    // Adjust speed gradually toward the target speed
    if (currentSpeed < targetSpeed) {
        currentSpeed += stepSize;
        if (currentSpeed > targetSpeed) currentSpeed = targetSpeed;
    } else if (currentSpeed > targetSpeed) {
        currentSpeed -= stepSize;
        if (currentSpeed < targetSpeed) currentSpeed = targetSpeed;
    }

    // Update the current speed in the system settings
    system_settings->currentSpeed = currentSpeed;

    // Return the current motor speed
    return currentSpeed;
}

void disable_motor_movment( ) {
    analogWrite(enA, 0); // Motor A drive
    analogWrite(enB, 0); // Motor B drive
}

void l298n_initial( SYSTEM_SETTINGS_STRUCT *system_settings ) {
    pinMode(enA, OUTPUT);
    pinMode(in1, OUTPUT);
    pinMode(in2, OUTPUT);
    pinMode(in3, OUTPUT);
    pinMode(in4, OUTPUT);
    pinMode(enB, OUTPUT);
    disable_motor_movment();
}

void direction( boolean direction ) {
    if ( !direction ){
        // Motor A reverse
        digitalWrite(in1, HIGH);
        digitalWrite(in2, LOW);
        // Motor B reverse
        digitalWrite(in3, HIGH);
        digitalWrite(in4, LOW);
    } else {
        // Motor A forward
        digitalWrite(in1, LOW);
        digitalWrite(in2, HIGH);
        // Motor B forward
        digitalWrite(in3, LOW);
        digitalWrite(in4, HIGH);
    }
}

void l298n_routine( SYSTEM_SETTINGS_STRUCT *system_settings ) {
    if ( system_settings->DEFINE_SELECTED ) { 
        
        bool directionVar = false ;
        if ( system_settings->INVERT_MOTOR_DRIVE_DIRECTION > 0 ){
            directionVar = true;
        }
        
        int local_Y_THIS_DEVICE = system_settings->LEFT_JOYSTICK_Y_THIS_DEVICE ;        
        
        // int local_Y_SOFT_START_THIS_DEVICE = local_Y_THIS_DEVICE ;

        // int targetSpeed = 200; // Input: Target speed (0-255)
        int K = 0 ; // 255 - system_settings->ACCELERATION_MULTIPLIER ;   // Input: Softness factor for starting (0-255)  - 0: NOT soft \ 255: Extermly-Soft
        int J = 0 ; // 255 - system_settings->ACCELERATION_MULTIPLIER ;   // Input: Softness factor for stopping (0-255)  - 0: NOT soft \ 255: Extermly-Soft

        int local_Y_SOFT_START_THIS_DEVICE = softStartStop(system_settings, local_Y_THIS_DEVICE, K, J);




        if ( local_Y_SOFT_START_THIS_DEVICE  > 128) {
            direction( directionVar );
            // direction(true);
            // translate incoming data to PWM reference
            motorSpeedA = customMap(local_Y_SOFT_START_THIS_DEVICE, 128, 255, 0,255);    // system_settings->SPEED_MAX_CAP ); 
            motorSpeedB = customMap(local_Y_SOFT_START_THIS_DEVICE, 128, 255, 0,255);    // system_settings->SPEED_MAX_CAP ); 

        } else if ( local_Y_SOFT_START_THIS_DEVICE  < 128) {
            direction( !directionVar );
            // direction(false);
            // translate incoming data to PWM reference
            motorSpeedA = customMap(local_Y_SOFT_START_THIS_DEVICE, 128, 0, 0,255);    // system_settings->SPEED_MAX_CAP ); 
            motorSpeedB = customMap(local_Y_SOFT_START_THIS_DEVICE, 128, 0, 0,255);    // system_settings->SPEED_MAX_CAP ); 

        } else {
            // Stop motor at 128
            local_Y_SOFT_START_THIS_DEVICE = 0;
            motorSpeedA = 0;
            motorSpeedB = 0;
        }

        // Stop noise at no movement status
        if (motorSpeedA < 70) {
            motorSpeedA = 0;
        }
        if (motorSpeedB < 70) {
            motorSpeedB = 0;
        }
        
    } else {
            motorSpeedA = 0;
            motorSpeedB = 0;
    }
    analogWrite(enA, motorSpeedA); // Motor A drive
    analogWrite(enB, motorSpeedB); // Motor B drive

}
#endif