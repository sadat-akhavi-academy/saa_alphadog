#ifndef LED_LIGHTS_WORKS_HPP
#define LED_LIGHTS_WORKS_HPP

#include "Arduino.h"
#include "modules/settings_n_structs.hpp"
#include <time.h>

const int ledPin_frontLight = 15;

void led_lights_initi( SYSTEM_SETTINGS_STRUCT *system_settings ) {
    pinMode (ledPin_frontLight, OUTPUT);
    analogWrite(system_settings->ledPin_frontLight, 255); // Turn on the LED
}

void led_fading(SYSTEM_SETTINGS_STRUCT *system_settings) {

    uint32_t now = millis(); // Get current time in microseconds

    // Check if it's time to update the value
    if ((now - system_settings->led_fading_last_update) >= (system_settings->led_fading_interval * 10)) {
        system_settings->led_fading_value += system_settings->led_fading_direction; // Adjust the value

        // Reverse direction if limits are reached
        if (system_settings->led_fading_value >= 75 || system_settings->led_fading_value <= 0) {
            system_settings->led_fading_direction *= -1;
        }

        // Update the last update time
        system_settings->led_fading_last_update = now;
    }
    // Serial.print(" system_settings->led_fading_value: ");
    // Serial.println(  system_settings->led_fading_value );
    analogWrite(system_settings->ledPin_frontLight, system_settings->led_fading_value); // LED fading working
     

}



void led_lights_routine(SYSTEM_SETTINGS_STRUCT *system_settings) {
    // Constants for flash behavior
    const uint32_t flash_interval = 500; // Interval for flashing (500ms)

    // Handle LED state based on input
    if (system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 51) {
        // Turn off LED
        analogWrite(system_settings->ledPin_frontLight, 0); // Turn off the LED
        system_settings->led_flash_active = false; // Stop flashing
        system_settings->led_on = false;          // LED is off

    } else if (system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 52) {
        // Flash LED
        system_settings->led_flash_active = true;
        system_settings->led_on = false; // Override ON state during flash
        
    } else if (system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 53) {
        // Turn on LED
        analogWrite(system_settings->ledPin_frontLight, 255); // Turn on the LED
        system_settings->led_flash_active = false; // Stop flashing
        system_settings->led_on = true;           // LED is on

    // Start fading
    } else if ( system_settings->ledFadingFlashEnable && !system_settings->ledFadingFlashInProgress ){ 
        // Store current LED value
        system_settings->led_fading_led_flash_active = system_settings->led_flash_active ;
        system_settings->led_fading_led_on = system_settings->led_on ;
        system_settings->led_flash_active = false ;

        // Start LED enable process
        system_settings->ledFadingFlashInProgress = true ;

    // Stop fading
    } else if ( !system_settings->ledFadingFlashEnable && system_settings->ledFadingFlashInProgress ){ 
        // Restore Previous LED value
        system_settings->led_flash_active = system_settings->led_fading_led_flash_active;
        system_settings->led_on  = system_settings->led_fading_led_on ;
        // Start LED enable process
        system_settings->ledFadingFlashInProgress = false ;
        system_settings->led_fading_value = 0;

        if (  system_settings->led_fading_led_on ) {
            analogWrite(system_settings->ledPin_frontLight, 255);
        } else {
            analogWrite(system_settings->ledPin_frontLight, 0);
        }
    }   
    

    // Handle flashing logic if active
    if (system_settings->led_flash_active) {
        uint32_t current_time = millis(); // Get current time
        if (current_time - system_settings->last_led_toggle_time >= flash_interval) {
            system_settings->last_led_toggle_time = current_time; // Update last toggle time
            system_settings->led_state = !system_settings->led_state; // Toggle LED state

            if (system_settings->led_state) {
                analogWrite(system_settings->ledPin_frontLight, 255); // LED on
            } else {
                analogWrite(system_settings->ledPin_frontLight, 0);   // LED off
            }
        }
    }


    // LED fading
    if ( system_settings->ledFadingFlashInProgress ){
        led_fading( system_settings );
    }

}


#endif