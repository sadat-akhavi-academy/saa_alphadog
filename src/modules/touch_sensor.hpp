#ifndef TOUCH_SENSOR_HPP
#define TOUCH_SENSOR_HPP

#include "modules/settings_n_structs.hpp"
#define TOUCH_SENSOR_PIN 0  // GPIO0 as touch sensor
// #define TOUCH_THRESHOLD 10   // Touch detection threshold
#define TOUCH_CHECK_INTERVAL 1000 // Time interval in milliseconds to check the touch sensor


// Function to get a filtered touch sensor value
int getFilteredTouchValue(int samples = 100) {
  int total = 0;
  for (int i = 0; i < samples; i++) {
    total += touchRead(TOUCH_SENSOR_PIN);
    delay(1);  // Minimal delay for sampling
  }
  return total / samples;
}




int Local_TOUCH_THRESHOLD = 0 ;
void touch_sensor_initial( SYSTEM_SETTINGS_STRUCT *system_settings ) {
    system_settings->touch_sensor_Startup_baseline = getFilteredTouchValue();
    Local_TOUCH_THRESHOLD = system_settings->touch_sensor_Startup_baseline/2;
}


void touch_sensor_routine( SYSTEM_SETTINGS_STRUCT *system_settings ) {
    Local_TOUCH_THRESHOLD = system_settings->touch_sensor_Startup_baseline/2;


    // Read the touch sensor value
    system_settings->touch_sensor_value = touchRead(TOUCH_SENSOR_PIN);

    // Determine if the touch sensor is active
    if (system_settings->touch_sensor_value < Local_TOUCH_THRESHOLD) {
        system_settings->touch_sensor_IO0_touched = true;
    } else {
        system_settings->touch_sensor_IO0_touched = false;
    }

    // Touch Flip Flop
    uint32_t currentMillis = millis();
    // Check if it's time to read the touch sensor
    if (    ( system_settings->touch_sensor_value < Local_TOUCH_THRESHOLD ) && 
            ( currentMillis - system_settings->lastTouchCheckTime >= TOUCH_CHECK_INTERVAL )) {
        system_settings->lastTouchCheckTime = currentMillis;
        system_settings->touch_sensor_delayed_flip_flop_IO0 = !system_settings->touch_sensor_delayed_flip_flop_IO0 ;
    }

}

#endif
