#ifndef PUSH_BUTTON_HPP
#define PUSH_BUTTON_HPP

#include "modules/bluetooth_works.hpp"
#include "modules/settings_n_structs.hpp"
#include "modules/EEPROMUtil.hpp"

#define PUSH_DOUBLED_BUTTON_HOLD_INTERVAL 5000 // Time interval in milliseconds to check the touch sensor
#define PUSH_BUTTON_01_HOLD_WAIT_BLE 500 // Time interval in milliseconds to check the touch sensor
#define PUSH_BUTTON_02_HOLD_WAIT_TRIMM 1000 // Time interval in milliseconds to check the touch sensor

void push_button_initial( SYSTEM_SETTINGS_STRUCT *system_settings ) {
    // Define pin numbers for the buttons
    #define BUTTON1_PIN 17
    #define BUTTON2_PIN 34

    // Configure button pins as inputs
    pinMode( BUTTON1_PIN, INPUT_PULLUP );
    pinMode( BUTTON2_PIN, INPUT_PULLUP );
}


void push_button_routine( SYSTEM_SETTINGS_STRUCT *system_settings ) {

    // push_button_1_state_BLE = Bluetooth start 
    // push_button_2_state_TRIM_RESET = Trim reset 
    // push_button_1_state_BLE && push_button_2_state_TRIM_RESET (HOLD) Reset all data in E2PROM and restart

    // Read the state of the buttons
    system_settings->push_button_1_state_BLE = digitalRead(BUTTON1_PIN);
    system_settings->push_button_2_state_TRIM_RESET = digitalRead(BUTTON2_PIN);


    uint32_t currentMillis = millis();
    // Check if both buttons are pressed (LOW state because of pull-up)
    system_settings->reset_push_button_sound_9901 = false ;

    // Double Button press
    if ( !system_settings->push_button_1_state_BLE && !system_settings->push_button_2_state_TRIM_RESET) {
        system_settings->reset_push_button_sound_9901 = true ;
        // Button delay 
        if ( currentMillis - system_settings->push_buttons_both_delay_CheckTime_factory_reset >= PUSH_DOUBLED_BUTTON_HOLD_INTERVAL ) {
            system_settings->push_buttons_both_delay_CheckTime_factory_reset = currentMillis;
            EEPROMUtil::eraseEEPROM();
            Serial.println(" >>>> Factory Reset Executed <<<<");
            system_settings->watch_dog_reset_device_due_2_radio_hw_failure = 9901;
        }
    } else {
        system_settings->push_buttons_both_delay_CheckTime_factory_reset = currentMillis;
    }
    
    // SW1 Button press
    if ( !system_settings->push_button_1_state_BLE ) {
        system_settings->button_1_pressed_buzzer_code_9904 = true ;
        // Button delay 
        if ( currentMillis - system_settings->push_button_1_delay_CheckTime_BLE_start >= PUSH_BUTTON_01_HOLD_WAIT_BLE ) {
            system_settings->push_button_1_delay_CheckTime_BLE_start = currentMillis;
            Serial.println(" >>>> SW1 - BLE Start Executed <<<<");
            BLE_start( system_settings );
        }
    } else {
        system_settings->push_button_1_delay_CheckTime_BLE_start = currentMillis;
    }
        
    // SW2 Button press
    if ( !system_settings->push_button_2_state_TRIM_RESET) {
        system_settings->button_2_pressed_9905 = true;
        // Button delay 
        if ( currentMillis - system_settings->push_button_2_delay_CheckTime_Trim_reset >= PUSH_BUTTON_02_HOLD_WAIT_TRIMM ) {
            system_settings->push_button_2_delay_CheckTime_Trim_reset = currentMillis;
            Serial.println(" >>>> SW2 - Trim Reset Execution <<<<");
            EEPROMUtil::writeParameterName("steerTrim", "90");
            system_settings->watch_dog_reset_device_due_2_radio_hw_failure = 9905;
        }
    } else {
        system_settings->push_button_2_delay_CheckTime_Trim_reset = currentMillis;
    }



}

#endif
