#ifndef BLUETOOTH_WORKS_HPP
#define BLUETOOTH_WORKS_HPP


#include "HardwareSerial.h"
#include "Arduino.h"
#include "BLEDevice.h"
#include "BLEServer.h"
#include "BLEUtils.h"
#include "BLE2902.h"
#include "modules/substring_util.hpp"
#include "modules/EEPROMUtil.hpp"
#include "iostream"
#include "cstring"
#include "base64.h"
#include <esp_system.h> // For rebooting ESP32

extern BLEServer *pServer;
extern BLECharacteristic *pCharacteristic;
extern BLEUUID mUUID;
extern BLEAdvertising *pAdvertising;
extern BLEService *pService ;
extern bool bleDeviceConnected ;
extern bool oldBLEDeviceConnected;
extern HardwareSerial Serial;

#define BLE_OFF_DELAY 1000 // Time interval in milliseconds to check the touch sensor

#define SERVICE_UUID        "0000feff-0000-1000-8000-00805f9b24fb" // ABX
// #define SERVICE_UUID        "0001feff-0000-1000-8000-00805f9b24fb" // ABXp
#define CHARACTERISTIC_UUID "4fafc102-0000-1000-8000-00805f9b24fb" // random UUID

#define RECIEVE_ARRAY_SIZE 1024
char* rx_DataCache_Assembled = new char[RECIEVE_ARRAY_SIZE](); // () initializes all elements to null
#define TRANSFER_ARRAY_SIZE 1024
char* tx_DataCache = new char[ TRANSFER_ARRAY_SIZE ](); // () initializes all elements to null

char* tx_InFlightData = new char[ TRANSFER_ARRAY_SIZE ](); // () initializes all elements to null
bool tx_Data_InFlight_InProgress = false;


#define BUZZER_PIN_BLE 32 // Replace with your actual buzzer pin
void beep(int frequency, int duration) {
    tone(BUZZER_PIN_BLE, frequency, duration); // Start the tone
    delay(duration);                       // Wait for the tone to finish
    noTone(BUZZER_PIN_BLE);                    // Stop the tone
}

//////////////////////////////////////////
// DR ?(HC: HealthCheck): Device Response
char* sendStr56 = new char[ TRANSFER_ARRAY_SIZE ](); 
char* tempCache = new char[ TRANSFER_ARRAY_SIZE ](); // () initializes all elements to null

//////////////////////////////////////////
//
#define sendingChunkSize 20 //max packet size
char* subStringDivided = new char[ sendingChunkSize ](); // () initializes all elements to null


//BLE server name
#define BLE_SVR_NAME "SadatAkhaviAcademy_ABX"

void disable_bluetooth(){
  // Disable Bluetooth
  Serial.println("Disabling Bluetooth...");
  btStop();
};

void sendingChunkData(char* sending_Chunk){
    
  pCharacteristic->setValue( sending_Chunk );
  pCharacteristic->notify();
  Serial.println("Sending Data...");
  delay(15);
};

char* extractSubstring(const char* source, int start, int end) {
    // Ensure start and end are within bounds
    int length = strlen(source);
    if (start >= length) return nullptr; // Nothing to extract
    if (end > length) end = length;

    // Calculate substring length
    int subLength = end - start;
    char* substring = new char[subLength + 1]; // +1 for null terminator

    strncpy(substring, source + start, subLength);
    substring[subLength] = '\0'; // Null-terminate the string
    return substring;
}


void incomingStringProcessing( char* receivingString ){

    Serial.print("receivingString>###>>  ");
    Serial.println(receivingString);


    //////////////////////////////////////////
    // DR ?(HC: HealthCheck): Device Response
    delete[] sendStr56;
    sendStr56 = new char[TRANSFER_ARRAY_SIZE]();

    delete[] tempCache;
    tempCache = new char[TRANSFER_ARRAY_SIZE]();

    String encoded = base64::encode( sendStr56 );
    
    if (strstr(receivingString, "030010_rebootBikeRequest")) {

        // Beep twice
        beep(2000, 850); // Frequency 1000 Hz, duration 200 ms
        delay(1000);      // Short gap between beeps
        beep(500, 200);
        delay(1000);      // Short gap between beeps
        beep(500, 200);
        delay(1000);      // Short gap between beeps
        beep(500, 200);
        delay(1000);

        // Reboot ESP32
        esp_restart();

        return;

    } else if (strstr(receivingString, "030020_erase2DefaultRequest")) {
        EEPROMUtil::eraseEEPROM();

        // Beep
        beep(3000, 2000); // Frequency 1000 Hz, duration 200 ms
        delay(1000);      // Short gap between beeps
        beep(3000, 500);
        delay(1000);
        beep(3000, 500);
        delay(1000);

        // Reboot ESP32
        esp_restart();

      return;

    } else if (strstr(receivingString, "03000x")) {

    } else if (strstr(receivingString, "03000x")) {

    } else if (strstr(receivingString, "03000x")) {

    } else if (strstr(receivingString, "03000x")) {

    } else if (strstr(receivingString, "030000_pullConfigRequest")) { // 030000_pullConfigRequest
      printf("Config extraction requested via BLE - config sent.\n");
        
      // Read parameters into buffers
      char storageVersion[64] = {0};
      EEPROMUtil::readParameter("storageVersion", storageVersion);
      char storageConfigVersion[64] = {0};
      EEPROMUtil::readParameter("storageConfigVersion", storageConfigVersion);
      char hardwareUUID[64] = {0};
      EEPROMUtil::readParameter("hardwareUUID", hardwareUUID);
      char deviceSelectionID[64] = {0};
      EEPROMUtil::readParameter("deviceSelectionID", deviceSelectionID);
      char steerTrim[64] = {0};
      EEPROMUtil::readParameter("steerTrim", steerTrim);
      char nickname[64] = {0};
      EEPROMUtil::readParameter("nickname", nickname);
      char rfChannel[64] = {0};
      EEPROMUtil::readParameter("rfChannel", rfChannel);
      char reverseMotorDirection[64] = {0};
      EEPROMUtil::readParameter("reverseMotorDirection", reverseMotorDirection);
      char reverseSteering[64] = {0};
      EEPROMUtil::readParameter("reverseSteering", reverseSteering);
      char boardGeneration[64] = {0};
      EEPROMUtil::readParameter("boardGeneration", boardGeneration);
      char boardVersion[64] = {0};
      EEPROMUtil::readParameter("boardVersion", boardVersion);
      char speedMaxCap[64] = {0};
      EEPROMUtil::readParameter("speedMaxCap", speedMaxCap);
      char acceleratorMultiplier[64] = {0};
      EEPROMUtil::readParameter("acceleratorMultiplier", acceleratorMultiplier);
      char speedBoostEnable[64] = {0};
      EEPROMUtil::readParameter("speedBoostEnable", speedBoostEnable);

      // Concatenate the strings
      strcat(tempCache, "0x0043-SWV_2025.01.01.19.00__SWV-SV_");
      strcat(tempCache, storageVersion);
      strcat(tempCache, "_SV-SCV_");
      strcat(tempCache, storageConfigVersion);
      strcat(tempCache, "_SCV-HU2ID_");
      strcat(tempCache, hardwareUUID);
      strcat(tempCache, "_HU2ID-RC_");
      strcat(tempCache, rfChannel);
      strcat(tempCache, "_RC-DSID_");
      strcat(tempCache, deviceSelectionID);
      strcat(tempCache, "_DSID-RV_");
      strcat(tempCache, reverseMotorDirection);
      strcat(tempCache, "_RV-RSTER_");
      strcat(tempCache, reverseSteering);
      strcat(tempCache, "_RSTER-BOGEN_");
      strcat(tempCache, boardGeneration);
      strcat(tempCache, "_BOGEN-BOVER_");
      strcat(tempCache, boardVersion);
      strcat(tempCache, "_BOVER-SPMAX_");
      strcat(tempCache, speedMaxCap);
      strcat(tempCache, "_SPMAX-ACCMU_");
      strcat(tempCache, acceleratorMultiplier);
      strcat(tempCache, "_ACCMU-SPEBOE_");
      strcat(tempCache, speedBoostEnable);
      strcat(tempCache, "_SPEBOE-STR_");
      strcat(tempCache, steerTrim);
      strcat(tempCache, "_STR-NN_");
      strcat(tempCache, nickname);
      strcat(tempCache, "_NN");

    } else if (strstr(receivingString, "1x0001")) { // Config update
        // Config update
        printf("New config received.\n");

        char rfChannel[64] = {0};
        if (extractSubstring(receivingString, "RC_", "_RC", rfChannel, sizeof(rfChannel))) {
            printf("Extracted value (rfChannel): %s\n", rfChannel);
            EEPROMUtil::writeParameterName("rfChannel", rfChannel); 
        }

        char deviceSelectionID[64] = {0};
        if (extractSubstring(receivingString, "DSID_", "_DSID", deviceSelectionID, sizeof(deviceSelectionID))) {
            printf("Extracted value (deviceSelectionID): %s\n", deviceSelectionID);
            EEPROMUtil::writeParameterName("deviceSelectionID", deviceSelectionID);
        }

        char reverseMotorDirection[64] = {0};
        if (extractSubstring(receivingString, "RV_", "_RV", reverseMotorDirection, sizeof(reverseMotorDirection))) {
            printf("Extracted value (reverseMotorDirection): %s\n", reverseMotorDirection);
            EEPROMUtil::writeParameterName("reverseMotorDirection", reverseMotorDirection); 
        }

        char reverseSteering[64] = {0};
        if (extractSubstring(receivingString, "RSTER_", "_RSTER", reverseSteering, sizeof(reverseSteering))) {
            printf("Extracted value (reverseSteering): %s\n", reverseSteering);
            EEPROMUtil::writeParameterName("reverseSteering", reverseSteering); 
        }

        char boardGeneration[64] = {0};
        if (extractSubstring(receivingString, "BOGEN_", "_BOGEN", boardGeneration, sizeof(boardGeneration))) {
            printf("Extracted value (boardGeneration): %s\n", boardGeneration);
            EEPROMUtil::writeParameterName("boardGeneration", boardGeneration); 
        }

        char boardVersion[64] = {0};
        if (extractSubstring(receivingString, "BOVER_", "_BOVER", boardVersion, sizeof(boardVersion))) {
            printf("Extracted value (boardVersion): %s\n", boardVersion);
            EEPROMUtil::writeParameterName("boardVersion", boardVersion); 
        }

        char speedMaxCap[64] = {0};
        if (extractSubstring(receivingString, "SPMAX_", "_SPMAX", speedMaxCap, sizeof(speedMaxCap))) {
            printf("Extracted value (speedMaxCap): %s\n", speedMaxCap);
            EEPROMUtil::writeParameterName("speedMaxCap", speedMaxCap); 
        }

        char acceleratorMultiplier[64] = {0};
        if (extractSubstring(receivingString, "ACCMU_", "_ACCMU", acceleratorMultiplier, sizeof(acceleratorMultiplier))) {
            printf("Extracted value (acceleratorMultiplier): %s\n", acceleratorMultiplier);
            EEPROMUtil::writeParameterName("acceleratorMultiplier", acceleratorMultiplier); 
        }

        char speedBoostEnable[64] = {0};
        if (extractSubstring(receivingString, "SPEBOE_", "_SPEBOE", speedBoostEnable, sizeof(speedBoostEnable))) {
            printf("Extracted value (speedBoostEnable): %s\n", speedBoostEnable);
            EEPROMUtil::writeParameterName("speedBoostEnable", speedBoostEnable); 
        }

        char steerTrim[64] = {0};
        if (extractSubstring(receivingString, "STR_", "_STR", steerTrim, sizeof(steerTrim))) {
            printf("Extracted value (steerTrim): %s\n", steerTrim);
            EEPROMUtil::writeParameterName("steerTrim", steerTrim);
        }
   
        char nickname[64] = {0};
        if (extractSubstring(receivingString, "NN_", "_NN", nickname, sizeof(nickname))) {
            printf("Extracted value (nickname): %s\n", nickname);
            EEPROMUtil::writeParameterName("nickname", nickname);
        }
      
        // Beep twice
        beep(1000, 200); // Frequency 1000 Hz, duration 200 ms
        delay(100);      // Short gap between beeps
        beep(1000, 200);

        // Pause before the next set of beeps
        delay(500);

        // Beep twice again
        beep(1000, 200);
        delay(100); // Short gap between beeps
        beep(1000, 200);

        // Pause before rebooting
        delay(1000);

        // Reboot ESP32
        esp_restart();

        return;
    }        
    strcat( tempCache, encoded.c_str() );
    strcpy( tx_DataCache , tempCache );
    return;
    
};

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      Serial.print(">>> onConnect  >>>  ");
      bleDeviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      Serial.print(">>> onDisconnect  >>>  ");
      bleDeviceConnected = false;

      // Restart advertising to allow reconnection.
      BLEDevice::getAdvertising()->start();
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      uint8_t* received_data = pCharacteristic->getData();
      char* pointerCharPointer = (char*) received_data;
        
      Serial.print("pointerCharPointer  >>>  ");
      Serial.println(pointerCharPointer);

      if ( strlen( pointerCharPointer ) == 0 ) { 
        return; 

      } else if ( strcmp(pointerCharPointer, "10_react_native_101" ) == 0 ){
        rx_DataCache_Assembled[0] = '\0';

      } else if ( strcmp(pointerCharPointer, "90_react_native_109" ) == 0 ){
        incomingStringProcessing( rx_DataCache_Assembled );
        rx_DataCache_Assembled[0] = '\0';

      } else {
        strcat(rx_DataCache_Assembled, pointerCharPointer); // concatenate str2 to str1
      }
      
    }
};

void stopBLE() {
    Serial.println("Stopping BLE stack...");
    pAdvertising->stop();
}

void deinitBLE() {
    Serial.println("deinit BLE stack...");
    BLEDevice::deinit(); // Deinitialize BLE stack
}

void restartBLE() {
    Serial.println("Restarting BLE stack...");
    BLEDevice::deinit(); // Deinitialize BLE stack
    BLEDevice::init( BLE_SVR_NAME ); // Reinitialize
}


void bluetooth_initialization( SYSTEM_SETTINGS_STRUCT *system_settings ) {

    ////////////////////////////////////////////////////////////////////////
    //
    try {
        BLEDevice::init( BLE_SVR_NAME );
        pServer = BLEDevice::createServer();
        if (!pServer) throw "Failed to create BLE server - 1.";
        pServer->setCallbacks(new MyServerCallbacks());

        // BLEService *pService = pServer->createService(SERVICE_UUID);
        pService = pServer->createService(SERVICE_UUID);
        if (!pService) throw "Failed to create BLE service - 2.";
        pCharacteristic = pService->createCharacteristic(
                                              CHARACTERISTIC_UUID,
                                              BLECharacteristic::PROPERTY_NOTIFY |
                                              BLECharacteristic::PROPERTY_WRITE |
                                              BLECharacteristic::PROPERTY_READ |
                                              BLECharacteristic::PROPERTY_INDICATE
                                            );
        if (!pCharacteristic) throw "Failed to create BLE characteristic.";

        pCharacteristic->addDescriptor(new BLE2902());
        pCharacteristic->setCallbacks(new MyCallbacks());

        // Start Service
        pService->start();

        pAdvertising = pServer->getAdvertising();
        if (!pAdvertising) throw "Failed to get BLE advertising instance.";
        pAdvertising->addServiceUUID(SERVICE_UUID);
        pAdvertising->setScanResponse(true);
        pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
        mUUID = BLEUUID::fromString(SERVICE_UUID);
        
    } catch (const char* errorMsg) {
        Serial.print("Error during Bluetooth initialization: ");
        Serial.println(errorMsg);
    } catch (...) {
        Serial.println("An unknown error occurred during Bluetooth initialization.");
    }
}

void BLE_start( SYSTEM_SETTINGS_STRUCT *system_settings ) {
    uint32_t currentMillis = millis();

    if ( !system_settings->advertisingStarted ) {
       system_settings->ble_buzzer_sound_ledFadingFlashEnable_9902 = true;
        system_settings->ledFadingFlashEnable = true;
        system_settings->isBluetoothEnabled = true;
        system_settings->advertisingStarted = true;
        system_settings->bluetoothStartTime = millis(); // Record the time when Bluetooth is enabled

        pAdvertising->start();
        Serial.println( "Waiting for a device to connect... ...");
    }
}


void BluetoothMainProcess( SYSTEM_SETTINGS_STRUCT *system_settings ) {

    // uint32_t currentMillis = millis();

    // if ( !system_settings->push_button_1_state_BLE && !system_settings->advertisingStarted ) {
    //    system_settings->ble_buzzer_sound_ledFadingFlashEnable_9902 = true;
    //     system_settings->ledFadingFlashEnable = true;
    //     system_settings->isBluetoothEnabled = true;
    //     system_settings->advertisingStarted = true;
    //     system_settings->bluetoothStartTime = millis(); // Record the time when Bluetooth is enabled

    //     pAdvertising->start();
    //     Serial.println( "Waiting for a device to connect... ...");
        
    // } else 

    if ( system_settings->isBluetoothEnabled && (millis() - system_settings->bluetoothStartTime >= 99000 ) ) {
      stopBLE();
      system_settings->isBluetoothEnabled = false;
      system_settings->advertisingStarted = false;
      system_settings->ledFadingFlashEnable = false;
    }

    // This hardware does not automatically send out data, it has to be instructed to send data by the device connected
    if ( bleDeviceConnected ) {
      int tx_CValueLength =  strlen( tx_DataCache );

      if ( !tx_CValueLength == 0 && !tx_Data_InFlight_InProgress ) { 
        tx_Data_InFlight_InProgress = true;

        strcpy( tx_InFlightData , tx_DataCache );
        // Emptying the incoming string
        tx_DataCache[0] = '\0';
        
        // Starting packet
        char starter_block[ sendingChunkSize ] = "100_esp32_000000101";
        sendingChunkData( starter_block );

        int tx_InFlightDataLength =  strlen( tx_InFlightData );
        // Data packets
        Serial.println("tx_InFlightData :::  ");
        Serial.println( tx_InFlightData );

      for (int i = 0; i < tx_InFlightDataLength; i += sendingChunkSize) {
          // Extract the substring for the current chunk
          char* mySubstring = extractSubstring(tx_InFlightData, i, i + sendingChunkSize);

          // Check if extraction succeeded
          if (mySubstring == nullptr) {
              Serial.println("Error: Failed to extract substring.");
              continue; // Skip this iteration
          }

          sendingChunkData(mySubstring);

          // Free dynamically allocated memory (if extractSubstring allocates)
          delete[] mySubstring; // or free(mySubstring) if malloc was used
      }

        // Finishing packet
        char finisher_block[ sendingChunkSize ] = "900_esp32_000000109";
        sendingChunkData(finisher_block);

        tx_Data_InFlight_InProgress = false;
        tx_InFlightData[0] = '\0';
      }
    }

    // Some change has happend to connection status of a device
    if ( bleDeviceConnected != oldBLEDeviceConnected ) {
        oldBLEDeviceConnected = bleDeviceConnected;
        if ( !bleDeviceConnected ) {
            pServer->startAdvertising();
            Serial.println("Waiting for a device to connect... ...");
        }
  }
  
}
#endif

