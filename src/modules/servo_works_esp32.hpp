
#ifndef SERVO_HPP
#define SERVO_HPP

#include "ESP32Servo.h"
#include "ESP32PWM.h"
#include "modules/settings_n_structs.hpp"

extern Servo ServoOutputA;

static const int servoAPin = 13;

// Slow changes - avoiding noise
static const int servo_slow_increment_delay_time = 15 ;
static const int servo_large_change_blocks = 3;
static const int servo_small_change_blocks = 1;

static const int servo_left_hard_stop = 5;
static const int servo_right_hard_stop = 175;

void servoESP32_initial( SYSTEM_SETTINGS_STRUCT *system_settings ) {
        system_settings->SERVO_DELAY_CURRENT_DEGREE = 90;
        system_settings->SERVO_DELAY_TIMER_MILLIS = millis();
        // Allow allocation timers
        ESP32PWM::allocateTimer(3);
        // ServoOutputA.setPeriodHertz(50);    // standard 50 hz servo
        ServoOutputA.attach(servoAPin, 500, 2400); // attaches the servo on pin
        ServoOutputA.attach(4, 500, 2400); // attaches the servo on pin
        ServoOutputA.attach(5, 500, 2400); // attaches the servo on pin
}


void servoESP32_routine( SYSTEM_SETTINGS_STRUCT *system_settings ) {

        ////////////////////////////////////////////////////////
        // Right Joystick - X
        int this_device_servo_final_target_angle ;
        if ( system_settings->KEYBOARD_LEFT_THIS_DEVICE == system_settings->DEVICE_SELECTION_ID
                || system_settings->KEYBOARD_LEFT_THIS_DEVICE == system_settings->BROADCAST_ADDRESS ) {


        // Serial.print(" RIGHT_JOYSTICK_X_THIS_DEVICE:");
        // Serial.print( system_settings->RIGHT_JOYSTICK_X_THIS_DEVICE );

        // Serial.println(" << ");

                if ( system_settings->INVERT_STEERING_DIRECTION > 0 ){
                        this_device_servo_final_target_angle = ( 180 - system_settings->RIGHT_JOYSTICK_X_THIS_DEVICE) + (system_settings->SERVO_TRIMM_DEGREE - 90);
                } else {
                        this_device_servo_final_target_angle = system_settings->RIGHT_JOYSTICK_X_THIS_DEVICE + (system_settings->SERVO_TRIMM_DEGREE - 90);
                }
                
        } else{
                this_device_servo_final_target_angle = system_settings->SERVO_TRIMM_DEGREE ;
        }

        int now_millis = millis();
        if ( now_millis - system_settings->SERVO_DELAY_TIMER_MILLIS  >= servo_slow_increment_delay_time ) {
                system_settings->SERVO_DELAY_TIMER_MILLIS = now_millis;
                if ( system_settings->SERVO_DELAY_CURRENT_DEGREE - this_device_servo_final_target_angle > servo_large_change_blocks ){
                        system_settings->SERVO_DELAY_CURRENT_DEGREE = system_settings->SERVO_DELAY_CURRENT_DEGREE - servo_large_change_blocks ;

                }else if ( this_device_servo_final_target_angle - system_settings->SERVO_DELAY_CURRENT_DEGREE > servo_large_change_blocks ){
                        system_settings->SERVO_DELAY_CURRENT_DEGREE = system_settings->SERVO_DELAY_CURRENT_DEGREE + servo_large_change_blocks ;

                }else if ( system_settings->SERVO_DELAY_CURRENT_DEGREE - this_device_servo_final_target_angle > servo_small_change_blocks ){
                        system_settings->SERVO_DELAY_CURRENT_DEGREE = system_settings->SERVO_DELAY_CURRENT_DEGREE - servo_small_change_blocks ;

                }else if ( this_device_servo_final_target_angle - system_settings->SERVO_DELAY_CURRENT_DEGREE > servo_small_change_blocks ){
                        system_settings->SERVO_DELAY_CURRENT_DEGREE = system_settings->SERVO_DELAY_CURRENT_DEGREE + servo_small_change_blocks ;

                } else {
                        system_settings->SERVO_DELAY_CURRENT_DEGREE = this_device_servo_final_target_angle;
                }
        }
        
        // Write to servo and apply hard limites 
        if ( system_settings->SERVO_DELAY_CURRENT_DEGREE < servo_left_hard_stop ){
                ServoOutputA.write( servo_left_hard_stop );
        } else if ( system_settings->SERVO_DELAY_CURRENT_DEGREE  > servo_right_hard_stop ){
                ServoOutputA.write( servo_right_hard_stop );
        } else  {
                ServoOutputA.write( system_settings->SERVO_DELAY_CURRENT_DEGREE );
        }
}

#endif
