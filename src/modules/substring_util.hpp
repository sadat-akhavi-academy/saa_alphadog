#include <string.h>
#include <stdio.h>

bool extractSubstring(const char *source, const char *startDelimiter, const char *endDelimiter, char *output, size_t outputSize) {
    const char *start = strstr(source, startDelimiter);
    const char *end = strstr(source, endDelimiter);

    if (start && end && start < end) {
        start += strlen(startDelimiter); // Move past the start delimiter
        size_t length = end - start;

        if (length < outputSize) {
            strncpy(output, start, length);
            output[length] = '\0'; // Null-terminate the string
            return true; // Success
        } else {
            printf("Error: Extracted string is too long for the output buffer.\n");
        }
    } else {
        printf("Error: Delimiters not found or invalid format.\n");
    }

    return false; // Failure
}
