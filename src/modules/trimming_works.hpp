#ifndef TRIMMING_WORKS_HPP
#define TRIMMING_WORKS_HPP

#include "Arduino.h"
#include "modules/settings_n_structs.hpp"
#include "modules/EEPROMUtil.hpp"
#include <stdint.h>
#include <time.h>

void trimming_works_routine(SYSTEM_SETTINGS_STRUCT *system_settings) {
    uint32_t current_time = clock(); // Get the current time (in clock ticks)

    // Check if enough time has passed since the last update
    if (current_time - system_settings->last_trim_update_time >= system_settings->trim_update_interval) {
        if ( system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 50 && system_settings->SERVO_TRIMM_DEGREE < 170 ) {
            system_settings->SERVO_TRIMM_DEGREE++;
            system_settings->last_trim_update_time = current_time; // Update the last update time

            // Steer Trim - 
            char intBuffer[64];
            snprintf(intBuffer, sizeof(intBuffer), "%d", system_settings->SERVO_TRIMM_DEGREE);
            EEPROMUtil::writeParameterName("steerTrim", intBuffer);
            
        } else if ( system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 49 && system_settings->SERVO_TRIMM_DEGREE > 10 ) {
            system_settings->SERVO_TRIMM_DEGREE--;
            system_settings->last_trim_update_time = current_time; // Update the last update time

            // Steer Trim - 
            char intBuffer[64];
            snprintf(intBuffer, sizeof(intBuffer), "%d", system_settings->SERVO_TRIMM_DEGREE);
            EEPROMUtil::writeParameterName("steerTrim", intBuffer);

        }
    }
}

#endif