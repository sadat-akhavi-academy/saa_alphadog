#ifndef ESP32_EEPROM_UTIL_H
#define ESP32_EEPROM_UTIL_H

#include <EEPROM.h>
#include "modules/generateUUID.hpp"
#include "modules/settings_n_structs.hpp"

#define EEPROM_SIZE 4096
#define PARAMETER_SIZE 64

class EEPROMUtil {
public:
    static void begin(SYSTEM_SETTINGS_STRUCT *system_settings) {
        EEPROM.begin(EEPROM_SIZE);
        initializeStorageVersion(system_settings);
        readAllParameters(system_settings);
    }

    static void readAllParameters(SYSTEM_SETTINGS_STRUCT *system_settings) {
        Serial.println("Reading All Parameters:");

        const char* parameterList[] = {
            "storageVersion", "storageConfigVersion", "hardwareUUID", "deviceSelectionID", 
            "steerTrim", "nickname", "rfChannel", "reverseMotorDirection", 
            "reverseSteering", "boardGeneration", "boardVersion", "speedMaxCap", "acceleratorMultiplier", "speedBoostEnable"
        };

        char buffer[PARAMETER_SIZE];

        for (int i = 0; i < sizeof(parameterList) / sizeof(parameterList[0]); ++i) {
            readParameter(parameterList[i], buffer);

            if (strcmp(parameterList[i], "storageVersion") == 0) {
                system_settings->storage_Version = atoi(buffer);
                Serial.printf("%s: %d\n", parameterList[i], system_settings->storage_Version);
            } else if (strcmp(parameterList[i], "storageConfigVersion") == 0) {
                system_settings->storage_Config_Version = atoi(buffer);
                Serial.printf("%s: %d\n", parameterList[i], system_settings->storage_Config_Version);
            } else if (strcmp(parameterList[i], "hardwareUUID") == 0) {
                strncpy(system_settings->hardware_UUID, buffer, PARAMETER_SIZE);
                Serial.printf("%s: %s\n", parameterList[i], system_settings->hardware_UUID);
            } else if (strcmp(parameterList[i], "deviceSelectionID") == 0) {
                system_settings->DEVICE_SELECTION_ID = buffer[0];
                Serial.printf("%s: %c\n", parameterList[i], system_settings->DEVICE_SELECTION_ID);
            } else if (strcmp(parameterList[i], "steerTrim") == 0) {
                system_settings->SERVO_TRIMM_DEGREE = atoi(buffer);
                Serial.printf("%s: %d\n", parameterList[i], system_settings->SERVO_TRIMM_DEGREE);
            } else if (strcmp(parameterList[i], "nickname") == 0) {
                strncpy(system_settings->NICK_NAME, buffer, PARAMETER_SIZE);
                Serial.printf("%s: %s\n", parameterList[i], system_settings->NICK_NAME);
            } else if (strcmp(parameterList[i], "rfChannel") == 0) {
                system_settings->RF_DATA_CHANNEL = atoi(buffer);
                Serial.printf("%s: %d\n", parameterList[i], system_settings->RF_DATA_CHANNEL);
            } else if (strcmp(parameterList[i], "reverseMotorDirection") == 0) {
                system_settings->INVERT_MOTOR_DRIVE_DIRECTION = atoi(buffer);
                Serial.printf("%s: %d\n", parameterList[i], system_settings->INVERT_MOTOR_DRIVE_DIRECTION);
            } else if (strcmp(parameterList[i], "reverseSteering") == 0) {
                system_settings->INVERT_STEERING_DIRECTION = atoi(buffer);
                Serial.printf("%s: %d\n", parameterList[i], system_settings->INVERT_STEERING_DIRECTION);
            } else if (strcmp(parameterList[i], "boardGeneration") == 0) {
                system_settings->BOARD_GENERATION = atoi(buffer);
                Serial.printf("%s: %d\n", parameterList[i], system_settings->BOARD_GENERATION);
            } else if (strcmp(parameterList[i], "boardVersion") == 0) {
                system_settings->BOARD_VERSION = atoi(buffer);
                Serial.printf("%s: %d\n", parameterList[i], system_settings->BOARD_VERSION);
            } else if (strcmp(parameterList[i], "speedMaxCap") == 0) {
                system_settings->SPEED_MAX_CAP = atoi(buffer);
                Serial.printf("%s: %d\n", parameterList[i], system_settings->SPEED_MAX_CAP);
            } else if (strcmp(parameterList[i], "acceleratorMultiplier") == 0) {
                system_settings->ACCELERATION_MULTIPLIER = atoi(buffer);
                Serial.printf("%s: %d\n", parameterList[i], system_settings->ACCELERATION_MULTIPLIER);
            } else if (strcmp(parameterList[i], "speedBoostEnable") == 0) {
                system_settings->SPEED_BOOST_ENABLE = atoi(buffer);
                Serial.printf("%s: %d\n", parameterList[i], system_settings->SPEED_BOOST_ENABLE);
            }
        }
    }

    static void writeParameterAddress(int startAddress, const char* value) {
        for (int i = 0; i < PARAMETER_SIZE; ++i) {
            EEPROM.write(startAddress + i, i < strlen(value) ? value[i] : '\0');
        }
        EEPROM.commit();
    }

    static void readParameter(int startAddress, char* buffer) {
        for (int i = 0; i < PARAMETER_SIZE; ++i) {
            buffer[i] = EEPROM.read(startAddress + i);
        }
        buffer[PARAMETER_SIZE - 1] = '\0';
    }

    static int getParameterAddress(const char* parameterName) {
        const char* parameterList[] = {
            "storageVersion", "storageConfigVersion", "hardwareUUID", "deviceSelectionID", 
            "steerTrim", "nickname", "rfChannel", "reverseMotorDirection", 
            "reverseSteering", "boardGeneration", "boardVersion", "speedMaxCap", "acceleratorMultiplier", "speedBoostEnable"
        };

        for (int i = 0; i < sizeof(parameterList) / sizeof(parameterList[0]); ++i) {
            if (strcmp(parameterList[i], parameterName) == 0) {
                return i * PARAMETER_SIZE;
            }
        }
        return -1; // Invalid parameter
    }

    static void writeParameterName(const char* parameterName, const char* value) {
        int address = getParameterAddress(parameterName);
        if (address >= 0) {
            writeParameterAddress(address, value);
            if (strcmp(parameterName, "storageConfigVersion") != 0) {
                incrementStorageConfigVersion();
            }
        }
    }

    static void readParameter(const char* parameterName, char* buffer) {
        int address = getParameterAddress(parameterName);
        if (address >= 0) {
            readParameter(address, buffer);
        } else {
            strcpy(buffer, "");
        }
    }

    static void eraseEEPROM() {
        for (int i = 0; i < EEPROM_SIZE; i++) {
            EEPROM.write(i, 0); // Write 0 to all EEPROM locations
        }
        if (EEPROM.commit()) {
            Serial.println("EEPROM erased successfully.");
        } else {
            Serial.println("Failed to erase EEPROM.");
        }
    }

private:
    static void initializeStorageVersion(SYSTEM_SETTINGS_STRUCT *system_settings) {
        char buffer[PARAMETER_SIZE];

        readParameter("storageVersion", buffer);
        int storage_Version = atoi(buffer);

        if (!(storage_Version > 17)) {
            writeParameterName("storageVersion", "18");
            writeParameterName("storageConfigVersion", "1");

            char uuid[37]; // UUID is 36 characters + null terminator
            generateUUID(uuid);
            writeParameterName("hardwareUUID", uuid);

            writeParameterName("deviceSelectionID", "1");
            writeParameterName("steerTrim", "90");
            writeParameterName("nickname", "My ABX");
            writeParameterName("rfChannel", "118");
            writeParameterName("reverseMotorDirection", "0");
            writeParameterName("reverseSteering", "0");
            writeParameterName("boardGeneration", "2");
            writeParameterName("boardVersion", "30");
            writeParameterName("speedMaxCap", "255");
            writeParameterName("acceleratorMultiplier", "255");
            writeParameterName("speedBoostEnable", "0");
        }
    }

    static void incrementStorageConfigVersion() {
        char buffer[PARAMETER_SIZE];
        readParameter("storageConfigVersion", buffer);
        int version = atoi(buffer);
        version++;
        char newVersion[PARAMETER_SIZE];
        snprintf(newVersion, PARAMETER_SIZE, "%d", version);
        writeParameterName("storageConfigVersion", newVersion);
    }
};

#endif // ESP32_EEPROM_UTIL_H
