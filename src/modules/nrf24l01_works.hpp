#ifndef NFG24L01_VAR
#define NFG24L01_VAR

#include "modules/settings_n_structs.hpp"
#include "modules/buzzer_works.hpp"
#include <SPI.h>
#include <RF24.h>
#include <RF24_config.h>

// rf24 pin ESP pin
// 1 ground
// 2 3.3V Note (10uF cap across ground and 3.3V)
// 3 (CE) 22
// 4 (CSN) 21
// 5 (SCK) 18
// 6 (MOSI) 23
// 7 (MISO) 19
#define CE_PIN  22
#define CSN_PIN 21

RF24 radio(CE_PIN, CSN_PIN);


bool healthCheck() {
    if (!radio.isChipConnected()) {
        Serial.println("NRF24L01 chip not connected!");
        return false;
    }
    return true;
}


void nrf24l01_initial( SYSTEM_SETTINGS_STRUCT *system_settings ) {
    // initialize the transceiver on the SPI bus
    if (!radio.begin()) {
        Serial.println(F("radio hw failure!!"));
        system_settings->watch_dog_reset_device_due_2_radio_hw_failure = 3226;
        passive_buzzer_error_noise();
    } else {
        Serial.println(F("radio hw Ok!!"));
    }
    
    radio.setDataRate(RF24_1MBPS); //(RF24_250KBPS|RF24_1MBPS|RF24_2MBPS)
    radio.openReadingPipe(0, 0xF0F0F0F0E1LL);    
    
    // Data channel
    radio.setChannel( system_settings->RF_DATA_CHANNEL ) ;

    // radio.setPALevel(RF24_PA_MIN); //(RF24_PA_MIN|RF24_PA_LOW|RF24_PA_HIGH|RF24_PA_MAX)
    radio.startListening();
}

void nrf24l01_routine( SYSTEM_SETTINGS_STRUCT *system_settings ) {
    
    // Health check of RF module, if fails multiple times trigger watch dog to reboot
    if ( system_settings->watch_dog_reset_device_due_2_radio_hw_failure__healthcheck_failed > 0 ){
        if ( healthCheck() ) {
            system_settings->watch_dog_reset_device_due_2_radio_hw_failure__healthcheck_failed = 0;
        } else {
            system_settings->watch_dog_reset_device_due_2_radio_hw_failure__healthcheck_failed++;
            if ( system_settings->watch_dog_reset_device_due_2_radio_hw_failure__healthcheck_failed > 100 ){
                system_settings->watch_dog_reset_device_due_2_radio_hw_failure = 5964;
                passive_buzzer_error_noise();
            }
            return;
        }
        
    }
    if (!healthCheck()) {
        Serial.println(F("radio hw health check failure!!"));
        system_settings->watch_dog_reset_device_due_2_radio_hw_failure__healthcheck_failed++;
        return;
    } 

    if (radio.available()) {
        char text[32] = "";
        radio.read(&text, sizeof(text));
        
        //////////////////////////////////////////////////////////////////////////////////
        unsigned char byte_0 = text[0];
        unsigned int unsignedInteger_0 = static_cast<unsigned int>(byte_0);
        system_settings->KEYBOARD_LEFT_RAW = unsignedInteger_0;

        //////////////////////////////////////////////////////////////////////////////////
        unsigned char byte_1 = text[1];
        unsigned int unsignedInteger_1 = static_cast<unsigned int>(byte_1);
        system_settings->LEFT_JOYSTICK_X_RAW = unsignedInteger_1;
        
        //////////////////////////////////////////////////////////////////////////////////
        unsigned char byte_2 = text[2];
        unsigned int unsignedInteger_2 = static_cast<unsigned int>(byte_2);
        system_settings->LEFT_JOYSTICK_Y_RAW = unsignedInteger_2 ;
        
        //////////////////////////////////////////////////////////////////////////////////
        unsigned char byte_3 = text[3];
        unsigned int unsignedInteger_3 = static_cast<unsigned int>(byte_3);
        system_settings->RIGHT_JOYSTICK_X_RAW = unsignedInteger_3*180/255 ;
        
        //////////////////////////////////////////////////////////////////////////////////
        unsigned char byte_4 = text[4];
        unsigned int unsignedInteger_4 = static_cast<unsigned int>(byte_4);
        system_settings->RIGHT_JOYSTICK_Y_RAW = unsignedInteger_4 ;
        
        //////////////////////////////////////////////////////////////////////////////////
        unsigned char byte_5 = text[5];
        unsigned int unsignedInteger_5 = static_cast<unsigned int>(byte_5);
        system_settings->KEYBOARD_RIGHT_RAW = unsignedInteger_5;

    system_settings->BIKE_REVERSING = false;
    
    if ( system_settings->KEYBOARD_LEFT_RAW == system_settings->DEVICE_SELECTION_ID
    || system_settings->KEYBOARD_LEFT_RAW == system_settings->BROADCAST_ADDRESS ) {
        
        system_settings->DEFINE_SELECTED = true;        
        system_settings->KEYBOARD_LEFT_THIS_DEVICE = system_settings->KEYBOARD_LEFT_RAW ;
        system_settings->LEFT_JOYSTICK_X_THIS_DEVICE = system_settings->LEFT_JOYSTICK_X_RAW ;
        system_settings->LEFT_JOYSTICK_Y_THIS_DEVICE = system_settings->LEFT_JOYSTICK_Y_RAW ;
        system_settings->LEFT_JOYSTICK_B_THIS_DEVICE = system_settings->LEFT_JOYSTICK_B_RAW ;
        system_settings->KEYBOARD_RIGHT_THIS_DEVICE = system_settings->KEYBOARD_RIGHT_RAW ;
        system_settings->RIGHT_JOYSTICK_X_THIS_DEVICE = system_settings->RIGHT_JOYSTICK_X_RAW ;
        system_settings->RIGHT_JOYSTICK_Y_THIS_DEVICE = system_settings->RIGHT_JOYSTICK_Y_RAW ;
        system_settings->RIGHT_JOYSTICK_B_THIS_DEVICE = system_settings->RIGHT_JOYSTICK_B_RAW ;

        if ( system_settings->LEFT_JOYSTICK_Y_THIS_DEVICE < 128 ) {
            system_settings->BIKE_REVERSING = true;
        }

    } else {
        system_settings->DEFINE_SELECTED = false;
        
        system_settings->KEYBOARD_LEFT_THIS_DEVICE = system_settings->KEYBOARD_LEFT_DEFAULT ;
        system_settings->LEFT_JOYSTICK_X_THIS_DEVICE = system_settings->LEFT_JOYSTICK_X_DEFAULT ;
        system_settings->LEFT_JOYSTICK_Y_THIS_DEVICE = system_settings->LEFT_JOYSTICK_Y_DEFAULT ;
        system_settings->LEFT_JOYSTICK_B_THIS_DEVICE = system_settings->LEFT_JOYSTICK_B_DEFAULT ;
        system_settings->KEYBOARD_RIGHT_THIS_DEVICE = system_settings->KEYBOARD_RIGHT_DEFAULT ;
        system_settings->RIGHT_JOYSTICK_X_THIS_DEVICE = system_settings->RIGHT_JOYSTICK_X_DEFAULT ;
        system_settings->RIGHT_JOYSTICK_Y_THIS_DEVICE = system_settings->RIGHT_JOYSTICK_Y_DEFAULT ;
        system_settings->RIGHT_JOYSTICK_B_THIS_DEVICE = system_settings->RIGHT_JOYSTICK_B_DEFAULT ;
    }


    }
}


#endif
