#include <Arduino.h>

// // Example
//     // Generate UUID
//     char uuid[37]; // UUID is 36 characters + null terminator
//     generateUUID(uuid);


void generateUUID(char* uuid) {
    // Define UUID format: xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx
    const char* hexChars = "0123456789abcdef";

    for (int i = 0; i < 36; i++) {
        if (i == 8 || i == 13 || i == 18 || i == 23) {
            uuid[i] = '-';
        } else if (i == 14) {
            uuid[i] = '4'; // UUID version 4
        } else if (i == 19) {
            uuid[i] = hexChars[(esp_random() & 0x0F) | 0x08]; // UUID variant
        } else {
            uuid[i] = hexChars[esp_random() % 16];
        }
    }
    uuid[36] = '\0'; // Null-terminate the string
}
