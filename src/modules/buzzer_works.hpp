#ifndef BUZZER_WORKS_INITIAL_HPP
#define BUZZER_WORKS_INITIAL_HPP

#include "Arduino.h"
#include "modules/settings_n_structs.hpp"

#include <stdint.h>
#include <time.h>
#include <stdbool.h>


#define NOTE_B0  31
#define NOTE_C1  33
#define NOTE_CS1 35
#define NOTE_D1  37
#define NOTE_DS1 39
#define NOTE_E1  41
#define NOTE_F1  44
#define NOTE_FS1 46
#define NOTE_G1  49
#define NOTE_GS1 52
#define NOTE_A1  55
#define NOTE_AS1 58
#define NOTE_B1  62
#define NOTE_C2  65
#define NOTE_CS2 69
#define NOTE_D2  73
#define NOTE_DS2 78
#define NOTE_E2  82
#define NOTE_F2  87
#define NOTE_FS2 93
#define NOTE_G2  98
#define NOTE_GS2 104
#define NOTE_A2  110
#define NOTE_AS2 117
#define NOTE_B2  123
#define NOTE_C3  131
#define NOTE_CS3 139
#define NOTE_D3  147
#define NOTE_DS3 156
#define NOTE_E3  165
#define NOTE_F3  175
#define NOTE_FS3 185
#define NOTE_G3  196
#define NOTE_GS3 208
#define NOTE_A3  220
#define NOTE_AS3 233
#define NOTE_B3  247
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976
#define NOTE_C7  2093
#define NOTE_CS7 2217
#define NOTE_D7  2349
#define NOTE_DS7 2489
#define NOTE_E7  2637
#define NOTE_F7  2794
#define NOTE_FS7 2960
#define NOTE_G7  3136
#define NOTE_GS7 3322
#define NOTE_A7  3520
#define NOTE_AS7 3729
#define NOTE_B7  3951
#define NOTE_C8  4186
#define NOTE_CS8 4435
#define NOTE_D8  4699
#define NOTE_DS8 4978

// buzzer pin output
const int BUZZER_PIN = 32; 

void passive_buzzer_initial( SYSTEM_SETTINGS_STRUCT *system_settings ) {
    pinMode(BUZZER_PIN, OUTPUT); 
}

void passive_buzzer_error_noise() {
    tone(BUZZER_PIN, 10, 100); 
    tone(BUZZER_PIN, 100, 500); 
    tone(BUZZER_PIN, 10, 100); 
    tone(BUZZER_PIN, 100, 500); 
    tone(BUZZER_PIN, 10, 100); 
    tone(BUZZER_PIN, 100, 500); 
    tone(BUZZER_PIN, 10, 100); 
    tone(BUZZER_PIN, 100, 500); 
    noTone(BUZZER_PIN); // Stop tone
}

void passive_buzzer_reverse_beep() {
    tone(BUZZER_PIN, 1000, 200); 
    noTone(BUZZER_PIN); // Stop tone
}

void passive_buzzer_stop_melody() {
    noTone(BUZZER_PIN);
}

void playMelody() {
    int melody[] = {
        NOTE_C4, NOTE_D4, NOTE_E4, NOTE_F4, NOTE_G4, NOTE_A4, NOTE_B4, NOTE_C5
    };
    
    // Define the durations of the notes
    int noteDurations[] = {
        4, 4, 4, 4, 4, 4, 4, 4
    };

    for (int i = 0; i < 8; i++) {
        int noteDuration = 1000 / noteDurations[i];
        tone(BUZZER_PIN, melody[i], noteDuration);
        delay(noteDuration * 1.1); 
        noTone(BUZZER_PIN); // Stop tone
    }
}
///////////////////////////////////////////////////////////////////////////////////////////
void handleHeartbeatBeep(SYSTEM_SETTINGS_STRUCT* system_settings, uint32_t current_time) {
 
    // Adjusted timing intervals for better audibility (in milliseconds)
    uint32_t heartbeat_short_on = 300;  // Duration of each short beep (on)
    uint32_t heartbeat_short_off = 300; // Pause between the two short beeps
    uint32_t heartbeat_long_off = 1000; // Long pause before the pattern repeats
    uint32_t heartbeat_duration = 10000; // Total duration of the heartbeat pattern (5 seconds)

    // Initialize the heartbeat pattern if phase is 0
    if (system_settings->heartbeat_phase == 0) {
        system_settings->heartbeat_start_time  = current_time; // Record the start time
        tone(system_settings->buzzerPin, 1000); // Start the first short beep
        system_settings->horn_state = true;
        system_settings->last_toggle_time_heartbeat = current_time;
        system_settings->heartbeat_phase = 1; // Move to the first pause
    }

    // Stop the heartbeat sound after 5 seconds
    if (current_time - system_settings->heartbeat_start_time  >= heartbeat_duration) {
        noTone(system_settings->buzzerPin); // Turn off the buzzer
        system_settings->horn_state = false;
        system_settings->heartbeat_phase = 0; // Reset the phase for the next call
        system_settings->horn_mode = 54 ;
        return;
    }

    // Handle the heartbeat phases
    switch (system_settings->heartbeat_phase) {
        case 1: // First short beep on
            if (current_time - system_settings->last_toggle_time_heartbeat >= heartbeat_short_on) {
                noTone(system_settings->buzzerPin); // Turn off the buzzer
                system_settings->horn_state = false;
                system_settings->last_toggle_time_heartbeat = current_time;
                system_settings->heartbeat_phase = 2; // Move to the short pause
            }
            break;

        case 2: // Short pause between two beeps
            if (current_time - system_settings->last_toggle_time_heartbeat >= heartbeat_short_off) {
                tone(system_settings->buzzerPin, 1000); // Start the second short beep
                system_settings->horn_state = true;
                system_settings->last_toggle_time_heartbeat = current_time;
                system_settings->heartbeat_phase = 3; // Move to the second beep
            }
            break;

        case 3: // Second short beep on
            if (current_time - system_settings->last_toggle_time_heartbeat >= heartbeat_short_on) {
                noTone(system_settings->buzzerPin); // Turn off the buzzer
                system_settings->horn_state = false;
                system_settings->last_toggle_time_heartbeat = current_time;
                system_settings->heartbeat_phase = 4; // Move to the long pause
            }
            break;

        case 4: // Long pause before restarting the pattern
            if (current_time - system_settings->last_toggle_time_heartbeat >= heartbeat_long_off) {
                system_settings->heartbeat_phase = 1; // Restart the pattern
                tone(system_settings->buzzerPin, 1000); // Start the next cycle
                system_settings->horn_state = true;
                system_settings->last_toggle_time_heartbeat = current_time;
            }
            break;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////

// Define variables for ON and OFF durations of the reversing beep
uint32_t reversing_beep_on_time = 500;   // Time in milliseconds the buzzer stays ON
uint32_t reversing_beep_off_time = 500; // Time in milliseconds the buzzer stays OFF

// Function to manage reversing beep
void manageReversingBeep(SYSTEM_SETTINGS_STRUCT *system_settings) {
    uint32_t current_time = millis();

    if (system_settings->BIKE_REVERSING) {
        // // If reversing and buzzer is not active, start beeping
        // if (!system_settings->horn_state) {
        //     system_settings->horn_state = true;
        //     tone(system_settings->buzzerPin, 1000); // Start beep
        //     system_settings->last_toggle_time_reverse = current_time;
        // }

        // Manage beep toggling between ON and OFF states
        if (system_settings->horn_state) {
            // If buzzer is ON, check if ON duration has elapsed
            if (current_time - system_settings->last_toggle_time_reverse >= reversing_beep_on_time) {
                noTone(system_settings->buzzerPin); // Turn beep OFF
                system_settings->horn_state = false;
                system_settings->last_toggle_time_reverse = current_time; // Update time
            }
        } else {
            // If buzzer is OFF, check if OFF duration has elapsed
            if (current_time - system_settings->last_toggle_time_reverse >= reversing_beep_off_time) {
                tone(system_settings->buzzerPin, 1000); // Turn beep ON
                system_settings->horn_state = true;
                system_settings->last_toggle_time_reverse = current_time; // Update time
            }
        }
    } else {
        // If not reversing, ensure the buzzer is OFF
        if (system_settings->horn_state) {
            noTone(system_settings->buzzerPin); // Turn off the buzzer
            system_settings->horn_state = false;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////
void horn_routine(SYSTEM_SETTINGS_STRUCT *system_settings) {

    // Define constants
    const uint32_t flash_beep_interval = 750;     // Interval for flash-beep (500ms)
    const uint32_t police_siren_duration = 5000; // Police siren duration (10 seconds)
    const uint32_t horn_duration_500 = 500;      // Horn duration (0.5 seconds)
    const uint32_t horn_duration_100 = 100;      // Horn duration (0.1 seconds)
    const uint32_t horn_duration_5_sec_long = 5000;      // Horn duration (5 seconds)
    const uint32_t horn_duration_short = 2500;    // Flash beep duration (5 seconds)
    const uint32_t siren_frequency_start = 400;   // Starting frequency for police siren
    const uint32_t siren_frequency_end = 1200;    // Ending frequency for police siren
    const uint32_t siren_step_time = 50;          // Time to increment/decrement frequency (50ms)
    const uint32_t music_note_duration = 150;     // Duration of each note in fun music (150ms)


    uint32_t current_time = millis(); // Get current time in milliseconds

    // Detect key press and update mode if a valid key is pressed
    if (system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 54) {
        system_settings->horn_mode = 54; // Turn off

    } else if (system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 55) {
        system_settings->horn_mode = 55; // Flash-beep

    } else if (system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 56) {
        system_settings->horn_mode = 56; // Police siren

    } else if (system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 57) {
        system_settings->horn_mode = 57; // Continuous horn

    } else if (system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 65) {
        system_settings->horn_mode = 65; 

    } else if (system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 66) {
        system_settings->horn_mode = 66; 

    } else if (system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 67) {
        system_settings->horn_mode = 67;

    } else if (system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 68) {
        system_settings->horn_mode = 68;

    } else if (system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 69) {
        system_settings->horn_mode = 69;

    } else if (system_settings->KEYBOARD_RIGHT_THIS_DEVICE == 70) {
        system_settings->horn_mode = 70;

    } else if ( system_settings->reset_push_button_sound_9901) {
        system_settings->horn_mode = 9901;
        system_settings->reset_push_button_sound_9901 = false ;

    } else if ( system_settings->ble_buzzer_sound_ledFadingFlashEnable_9902 ) {
        system_settings->horn_mode = 9902;
        system_settings->ble_buzzer_sound_ledFadingFlashEnable_9902 = false ;

    } else if ( system_settings->ble_buzzer_sound_ledFadingFlashEnable_9903 ) {
        system_settings->horn_mode = 9903;
        system_settings->ble_buzzer_sound_ledFadingFlashEnable_9903 = false ;

    } else if ( system_settings->button_1_pressed_buzzer_code_9904 ) {
        system_settings->horn_mode = 9904;
        system_settings->button_1_pressed_buzzer_code_9904 = false ;

    } else if ( system_settings->button_2_pressed_9905 ) {
        system_settings->horn_mode = 9905;
        system_settings->button_2_pressed_9905 = false ;
    }


    manageReversingBeep(system_settings);

    switch (system_settings->horn_mode) {
        case 54: // Turn off horn
            noTone(system_settings->buzzerPin);
            system_settings->horn_active = false;
            system_settings->horn_mode = 0; // Reset mode to idle
            system_settings->horn_state = false;
            system_settings->heartbeat_phase = 0; // Reset the phase for the next call

            break;

        case 55: // Flash-beep for 5 seconds
            handleHeartbeatBeep(system_settings, current_time );            
            break;

        case 56: // Police siren for 10 seconds
            if (!system_settings->horn_active) {
                system_settings->horn_active = true;
                system_settings->horn_start_time = current_time;
                system_settings->siren_frequency = siren_frequency_start;
                system_settings->siren_direction = true; // Increasing frequency
                system_settings->last_toggle_time = current_time;
            }
            if (current_time - system_settings->horn_start_time < police_siren_duration) {
                if (current_time - system_settings->last_toggle_time >= siren_step_time) {
                    system_settings->last_toggle_time = current_time;
                    // Increment or decrement frequency
                    if (system_settings->siren_direction) {
                        system_settings->siren_frequency += 50; // Increase frequency
                        if (system_settings->siren_frequency >= siren_frequency_end) {
                            system_settings->siren_direction = false; // Reverse direction
                        }
                    } else {
                        system_settings->siren_frequency -= 50; // Decrease frequency
                        if (system_settings->siren_frequency <= siren_frequency_start) {
                            system_settings->siren_direction = true; // Reverse direction
                        }
                    }
                    tone(system_settings->buzzerPin, system_settings->siren_frequency); // Update frequency
                }
            } else {
                system_settings->horn_mode = 54; // Auto-turn off after 10 seconds
            }
            break;

        case 57: // Continuous horn for 1000 seconds
            if (!system_settings->horn_active) {
                system_settings->horn_active = true;
                system_settings->horn_start_time = current_time;
                tone(system_settings->buzzerPin, 1000); // Continuous horn ON
            }
            if (current_time - system_settings->horn_start_time >= 25) {
                system_settings->horn_mode = 54; // Auto-turn off after 1000 seconds
            }
            break;


        case 65: { // Fun music
            static uint8_t note_index = 0;
            static const uint16_t music_notes[] = {330, 330, 330, 262, 330, 392, 440, 392, 330, 262, 294, 330}; // Notes in Hz
            static const uint16_t note_durations[] = {150, 150, 150, 150, 150, 200, 300, 150, 150, 150, 150, 400}; // Durations in ms
            static const uint8_t num_notes = sizeof(music_notes) / sizeof(music_notes[0]);

            if (!system_settings->horn_active) {
                system_settings->horn_active = true;
                system_settings->horn_start_time = current_time;
                system_settings->last_toggle_time = current_time;
                note_index = 0;
            }

            if (note_index < num_notes && current_time - system_settings->last_toggle_time >= note_durations[note_index]) {
                system_settings->last_toggle_time = current_time;
                tone(system_settings->buzzerPin, music_notes[note_index]);
                note_index++;
            }

            if (note_index >= num_notes) {
                system_settings->horn_mode = 54; // Auto-turn off after playing music
                noTone(system_settings->buzzerPin);
            }
        } break;

      case 66: { // Fun music
            static uint8_t note_index = 0;
            static const uint16_t music_notes[] = {440, 494, 523, 440, 392, 440, 494}; // Notes in Hz
            static const uint8_t num_notes = sizeof(music_notes) / sizeof(music_notes[0]);

            if (!system_settings->horn_active) {
                system_settings->horn_active = true;
                system_settings->horn_start_time = current_time;
                system_settings->last_toggle_time = current_time;
                note_index = 0;
            }

            if (note_index < num_notes && current_time - system_settings->last_toggle_time >= music_note_duration) {
                system_settings->last_toggle_time = current_time;
                tone(system_settings->buzzerPin, music_notes[note_index]);
                note_index++;
            }

            if (note_index >= num_notes) {
                system_settings->horn_mode = 54; // Auto-turn off after playing music
                noTone(system_settings->buzzerPin);
            }
        } break; 


        case 67: { // Fun music
            static uint8_t note_index = 0;
            static const uint16_t music_notes[] = {659, 698, 784, 880, 784, 698, 659, 587, 659}; // Notes in Hz
            static const uint16_t note_durations[] = {200, 200, 200, 300, 200, 200, 200, 300, 400}; // Durations in ms
            static const uint8_t num_notes = sizeof(music_notes) / sizeof(music_notes[0]);

            if (!system_settings->horn_active) {
                system_settings->horn_active = true;
                system_settings->horn_start_time = current_time;
                system_settings->last_toggle_time = current_time;
                note_index = 0;
            }

            if (note_index < num_notes && current_time - system_settings->last_toggle_time >= note_durations[note_index]) {
                system_settings->last_toggle_time = current_time;
                tone(system_settings->buzzerPin, music_notes[note_index]);
                note_index++;
            }

            if (note_index >= num_notes) {
                system_settings->horn_mode = 54; // Auto-turn off after playing music
                noTone(system_settings->buzzerPin);
            }
        } break;

        case 68: { // Police siren-like wail-up
            static uint32_t last_toggle_time = 0;
            static uint16_t current_frequency = 400; // Start low frequency
            static const uint16_t max_frequency = 2000; // Maximum frequency (Hz)
            static const uint16_t min_frequency = 400;  // Minimum frequency (Hz)
            static const uint32_t siren_rise_speed = 50; // Time to increase/decrease frequency (ms)
            static const uint32_t siren_fall_speed = 20; // Time to fall frequency faster (ms)

            // Restart siren when triggered again
            if (!system_settings->horn_active) {
                system_settings->horn_active = true;
                system_settings->horn_start_time = current_time;
                system_settings->last_toggle_time = current_time;
                current_frequency = 400; // Reset frequency to low when triggered
                tone(system_settings->buzzerPin, current_frequency); // Start tone immediately
            }

            // Rising frequency (wail-up)
            if (current_time - system_settings->last_toggle_time >= siren_rise_speed && current_frequency < max_frequency) {
                current_frequency += 100; // Increase frequency by 100Hz
                tone(system_settings->buzzerPin, current_frequency);
                system_settings->last_toggle_time = current_time;
            }
            // After reaching max frequency, start falling frequency
            else if (current_frequency >= max_frequency) {
                if (current_time - system_settings->last_toggle_time >= siren_fall_speed) {
                    current_frequency -= 200; // Decrease frequency by 200Hz
                    if (current_frequency < min_frequency) {
                        current_frequency = min_frequency; // Don't go below minimum frequency
                    }
                    tone(system_settings->buzzerPin, current_frequency);
                    system_settings->last_toggle_time = current_time;
                }
            }

            // Stop after 10 seconds (or desired duration)
            if (current_time - system_settings->horn_start_time >= 5000) {
                noTone(system_settings->buzzerPin);
                system_settings->horn_mode = 54; // Stop the siren after 10 seconds
                system_settings->horn_active = false; // Reset the horn state to allow restart
            }
        } break;

        case 69: { // Ambulance siren
            static uint32_t last_toggle_time = 0;
            static uint8_t siren_direction = 0; // 0 for low to high, 1 for high to low
            static const uint16_t low_frequency = 400;  // Low frequency (Hz)
            static const uint16_t high_frequency = 1200; // High frequency (Hz)
            static const uint32_t siren_speed = 100; // Speed of frequency change (in ms)

            // Start siren
            if (!system_settings->horn_active) {
                system_settings->horn_active = true;
                system_settings->horn_start_time = current_time;
                system_settings->last_toggle_time = current_time;
            }

            if (current_time - system_settings->last_toggle_time >= siren_speed) {
                system_settings->last_toggle_time = current_time;

                // Toggle between low and high frequencies
                if (siren_direction == 0) {
                    tone(system_settings->buzzerPin, low_frequency); // Play low frequency
                    siren_direction = 1;  // Change direction to high
                } else {
                    tone(system_settings->buzzerPin, high_frequency); // Play high frequency
                    siren_direction = 0;  // Change direction to low
                }
            }

            // Stop after 10 seconds (or desired duration)
            if (current_time - system_settings->horn_start_time >= 5000) {
                noTone(system_settings->buzzerPin);
                system_settings->horn_mode = 54; // Stop the siren after 10 seconds
            }
        } break;

        case 70: { // Continuous Wail-Up and Wail-Down Siren
            static uint32_t last_toggle_time = 0;
            static uint16_t current_frequency = 400; // Start low frequency
            static const uint16_t max_frequency = 2000; // Maximum frequency (Hz)
            static const uint16_t min_frequency = 400;  // Minimum frequency (Hz)
            static const uint32_t siren_speed = 50; // Speed at which the frequency changes (ms)
            static bool rising = true; // Flag to indicate if the frequency is rising or falling

            // Start siren
            if (!system_settings->horn_active) {
                system_settings->horn_active = true;
                system_settings->horn_start_time = current_time;
                system_settings->last_toggle_time = current_time;
            }

            // Control the rising and falling of frequency
            if (current_time - system_settings->last_toggle_time >= siren_speed) {
                system_settings->last_toggle_time = current_time;

                if (rising) {
                    current_frequency += 100; // Increase frequency by 100Hz
                    if (current_frequency >= max_frequency) {
                        current_frequency = max_frequency; // Cap at max frequency
                        rising = false; // Start falling after reaching max frequency
                    }
                } else {
                    current_frequency -= 200; // Decrease frequency by 200Hz
                    if (current_frequency <= min_frequency) {
                        current_frequency = min_frequency; // Cap at min frequency
                        rising = true; // Start rising again after reaching min frequency
                    }
                }

                tone(system_settings->buzzerPin, current_frequency);
            }

            // Stop after 10 seconds
            if (current_time - system_settings->horn_start_time >= 5000) {
                noTone(system_settings->buzzerPin);
                system_settings->horn_mode = 54; // Stop the siren after 10 seconds
            }
        } break;

        case 9901: // Continuous horn for 1000 seconds
            if (!system_settings->horn_active) {
                system_settings->horn_active = true;
                system_settings->horn_start_time = current_time;
                tone(system_settings->buzzerPin, 3500); // Continuous horn ON
            }
            if (current_time - system_settings->horn_start_time >= horn_duration_5_sec_long) {
                system_settings->horn_mode = 54; // Auto-turn off after seconds
            }
            break;

        case 9902: // Continuous horn for 1000 seconds
            if (!system_settings->horn_active) {
                system_settings->horn_active = true;
                system_settings->horn_start_time = current_time;
                tone(system_settings->buzzerPin, 1500); // Continuous horn ON
            }
            if (current_time - system_settings->horn_start_time >= horn_duration_500) {
                system_settings->horn_mode = 54; // Auto-turn off after seconds
            }
            break;

        case 9903: // Continuous horn for 1000 seconds
            if (!system_settings->horn_active) {
                system_settings->horn_active = true;
                system_settings->horn_start_time = current_time;
                tone(system_settings->buzzerPin, 3000); // Continuous horn ON
            }
            if (current_time - system_settings->horn_start_time >= horn_duration_500) {
                system_settings->horn_mode = 54; // Auto-turn off after seconds
            }
            break;

        case 9904: // Continuous horn for 1000 seconds
            if (!system_settings->horn_active) {
                system_settings->horn_active = true;
                system_settings->horn_start_time = current_time;
                tone(system_settings->buzzerPin, 300); // Continuous horn ON
            }
            if (current_time - system_settings->horn_start_time >= horn_duration_100) {
                system_settings->horn_mode = 54; // Auto-turn off after seconds
            }
            break;

        case 9905: // Continuous horn for 1000 seconds
            if (!system_settings->horn_active) {
                system_settings->horn_active = true;
                system_settings->horn_start_time = current_time;
                tone(system_settings->buzzerPin, 1750); // Continuous horn ON
            }
            if (current_time - system_settings->horn_start_time >= horn_duration_100) {
                system_settings->horn_mode = 54; // Auto-turn off after seconds
            }
            break;            
        default:
            // No action for unhandled modes
            break;
    }
}

 
#endif
