// https://iotassistant.io/esp32/enable-hardware-watchdog-timer-esp32-arduino-ide/

#ifndef WATCH_DOG_ESP32_HPP
#define WATCH_DOG_ESP32_HPP

#include "esp_task_wdt.h"
#include "modules/settings_n_structs.hpp"
#include "modules/l298n_works.hpp"

// 5 seconds WDT
#define WDT_TIMEOUT 3

void watch_dog_initial( SYSTEM_SETTINGS_STRUCT *system_settings ) {
    system_settings->WATCH_DOG_TIMER_MILLIS = millis();
    esp_task_wdt_init(WDT_TIMEOUT, true); //enable panic so ESP32 restarts
    esp_task_wdt_add(NULL); //add current thread to WDT watch
}

void watch_dog_routine( SYSTEM_SETTINGS_STRUCT *system_settings ) {
  // By passing  watch dog reset - failure a process in software - failure error code displayed below
  if ( system_settings->watch_dog_reset_device_due_2_radio_hw_failure != 0 ){
    Serial.print(" WDT warning ... - watch_dog_reset_device_due_2_radio_hw_failure - value : ");
    Serial.print( system_settings->watch_dog_reset_device_due_2_radio_hw_failure );
    Serial.println("   ");
    disable_motor_movment();
    // Reboot ESP32
    esp_restart();
    return;
  }
  // Resetting watch dog
  if (millis() - system_settings->WATCH_DOG_TIMER_MILLIS  >= 900 ) {
      // Serial.println("Resetting WDT...");
      esp_task_wdt_reset();
      system_settings->WATCH_DOG_TIMER_MILLIS  = millis();
  }
}
#endif