#ifndef BATTERY_WORKS_HPP
#define BATTERY_WORKS_HPP

#include "Arduino.h"
#include "modules/settings_n_structs.hpp"

// const int battery_voltage_record = 39;
// const int battery_voltage_record = 34;
const int battery_voltage_record = 35;

int voltage_read(){
    // Read voltage and convert
    int R1 = 33000;
    int R2 = 10000;
    int voltage_adc_read = analogRead( battery_voltage_record );

    // Serial.print(" BAT_VOLT:");
    // Serial.print( voltage_adc_read );

    // Serial.println(" << ");

    return voltage_adc_read*((R1+R2)*3.2)/(4095*R2) ;
}


void battery_works_initi( SYSTEM_SETTINGS_STRUCT *system_settings ) {
    // pinMode(battery_voltage_record, OUTPUT);
    
    // Configure ADC
    analogReadResolution(12); // Set resolution to 12-bit (0-4095)
    analogSetAttenuation(ADC_11db); // Set attenuation for full-scale voltage up to ~3.3V
    
}

void battery_works_routine( SYSTEM_SETTINGS_STRUCT *system_settings ) {
    system_settings->BATTERY_VOLTAGE_READ = voltage_read() ;
}
#endif