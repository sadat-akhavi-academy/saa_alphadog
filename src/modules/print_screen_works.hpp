#ifndef PRINT_SCREEN_HPP
#define PRINT_SCREEN_HPP

#include "Arduino.h"
#include "string.h"
#include "modules/settings_n_structs.hpp"

void print_console_routine( SYSTEM_SETTINGS_STRUCT *system_settings ) {
        
        Serial.print("ABX KLB:");
        Serial.print(system_settings->KEYBOARD_LEFT_THIS_DEVICE );
        Serial.print(" JXL:");
        Serial.print( system_settings->LEFT_JOYSTICK_X_THIS_DEVICE  );
        Serial.print(" JYL:");
        Serial.print( system_settings->LEFT_JOYSTICK_Y_THIS_DEVICE );
        Serial.print(" JXR:");
        Serial.print(  system_settings->RIGHT_JOYSTICK_X_THIS_DEVICE );
        Serial.print(" JYR:");
        Serial.print(system_settings->RIGHT_JOYSTICK_Y_THIS_DEVICE);
        Serial.print(" KBR:");
        Serial.print(system_settings->KEYBOARD_RIGHT_THIS_DEVICE );

        Serial.print(" TRIMM_DEG:");
        Serial.print( system_settings->SERVO_TRIMM_DEGREE );

        Serial.print(" SERVO_DELAYED_DEG:");
        Serial.print( system_settings->SERVO_DELAY_CURRENT_DEGREE );

        Serial.print(" LED_ON:");
        Serial.print(system_settings->led_on);

        Serial.print(" LED_FLASH:");
        Serial.print(system_settings->led_flash_active);

        Serial.print(" LED_STATE:");
        Serial.print(system_settings->led_state);

        Serial.print(" BAT_VOLT:");
        Serial.print( system_settings->BATTERY_VOLTAGE_READ );

        Serial.print(" BLE En: ");
        Serial.print( system_settings->isBluetoothEnabled );


        Serial.print(" Touch Tshld: ");
        Serial.print( system_settings->touch_sensor_Startup_baseline );

        Serial.print(" Touch: ");
        Serial.print( system_settings->touch_sensor_IO0_touched );
        
        Serial.print(" TouchValue: ");
        Serial.print( system_settings->touch_sensor_value );

        Serial.print(" But1: ");
        Serial.print( system_settings->push_button_1_state_BLE );

        Serial.print(" But2: ");
        Serial.print( system_settings->push_button_2_state_TRIM_RESET );

        Serial.print(" RF: ");
        Serial.print( system_settings->RF_DATA_CHANNEL );

        Serial.print(" SWv:");
        Serial.print( system_settings->Software_Version );

        Serial.print(" STv:");
        Serial.print( system_settings->storage_Version );

        Serial.println(" << ");



}
#endif
