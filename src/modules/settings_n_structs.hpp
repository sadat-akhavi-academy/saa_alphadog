#ifndef settings_n_structs_VAR
#define settings_n_structs_VAR

#include "ESP32Servo.h"

using namespace std;

struct SYSTEM_SETTINGS_STRUCT{   
    int touch_sensor_Startup_baseline = 0;
    int touch_sensor_value = 0 ;
    bool push_button_1_state_BLE = false ; 
    bool push_button_2_state_TRIM_RESET = false ; 
    bool advertisingStarted = false;
    bool touch_sensor_IO0_touched = false;
    bool touch_sensor_delayed_flip_flop_IO0 = false;
    bool ble_advertising_started = false; // Track advertising state
    int ble_stop_delay = 0 ;
    bool ble_buzzer_sound_ledFadingFlashEnable_9902 = false;
    bool ble_buzzer_sound_ledFadingFlashEnable_9903 = false;
    bool button_1_pressed_buzzer_code_9904 = false;
    bool button_2_pressed_9905 = false;

    bool ledFadingFlashEnable = false;
    bool ledFadingFlashInProgress = false;
    int led_fading_store_prior_state = 255 ;

    bool led_fading_led_flash_active = false; // Stop flashing
    bool led_fading_led_on = false;          // LED is off

    int led_fading_interval; 
    int led_fading_value = 0 ;
    int led_fading_direction = 1;
    uint32_t led_fading_last_update;
    uint32_t lastTouchCheckTime = 0;
    
    
    uint32_t push_button_1_delay_CheckTime_BLE_start = 0;
    uint32_t push_button_2_delay_CheckTime_Trim_reset = 0;
    uint32_t push_buttons_both_delay_CheckTime_factory_reset = 0;
    bool reset_push_button_sound_9901 = false;

    // Timer variables
    unsigned long bluetoothStartTime = 0;
    bool isBluetoothEnabled = false;

    // The nRF24L01 operates on multiple radio frequency (RF) channels, typically 126 in the 2.4 GHz ISM band. However, the exact number might vary slightly depending on the specific variant of the module. You can consult the datasheet for your particular nRF24L01 for the confirmed channel count.
    // Channel Isolation: As mentioned previously, using closely spaced channels can lead to interference. If you're using multiple nRF24L01 modules, it's recommended to choose channels that are at least 5 channels apart for better isolation.
    // The nRF24L01 module typically supports channels ranging from 0 to 125 (although the exact range might vary slightly depending on the specific module variant).
    // You can use decimal or hexadecimal values within this range.
    // https://arduinolearn.github.io/nrf1.html
    // https://www.allaboutcircuits.com/uploads/articles/Bluetooth_and_WLAN_frequencies.jpg
    // Channel example:
    //      radio.setChannel(42); (decimal) sets the channel to number 42.
    //      radio.setChannel(0x2A); (hexadecimal) sets the channel to number 42 (hexadecimal 2A is equivalent to decimal 42).
    // int RF_DATA_CHANNEL = 118 ; // radio.setChannel(0x76) ; // 118 decimal is equivlant to 0x76

    // Device Selector
    char DEVICE_SELECTION_ID = '1';
    char BROADCAST_ADDRESS = 'F';
    bool DEFINE_SELECTED = false ;

    int watch_dog_reset_device_due_2_radio_hw_failure = 0;  // this enables device to be able to perfrom some basic tasks - trim wheel for toy assembly
    int watch_dog_reset_device_due_2_radio_hw_failure__healthcheck_failed = 0;

    const int L298_enA_PORT = 33; 
    const int L298_in1_PORT = 25;
    const int L298_in2_PORT = 26;
    const int L298_in3_PORT = 27;
    const int L298_in4_PORT = 14;
    const int L298_enB_PORT = 12;

    char Software_Version[64] = "2025.01.01.19.00_"; // NOTE: Updatre this in Bluetooth as well.


    int storage_Version;
    int storage_Config_Version;
    char hardware_UUID[64];
    
    char NICK_NAME[64];

    // char rf_Channel[64];
    int RF_DATA_CHANNEL = 118 ;// Storage OverWrites this
    
    int INVERT_MOTOR_DRIVE_DIRECTION = 0 ; // Storage OverWrites this
    int INVERT_STEERING_DIRECTION = 0 ; 

    int BOARD_GENERATION = 2;   // MK2  // Storage OverWrites this
    int BOARD_VERSION = 30;     // x.30 // Storage OverWrites this

    int SPEED_MAX_CAP = 255;  // Storage OverWrites this
    int ACCELERATION_MULTIPLIER = 255; // Storage OverWrites this
    int SPEED_BOOST_ENABLE = 0; // Storage OverWrites this



    bool BIKE_REVERSING = false ;

    /// Left side controllers
    const int KEYBOARD_LEFT_DEFAULT = 0;
    int KEYBOARD_LEFT_RAW = 0;
    int KEYBOARD_LEFT_THIS_DEVICE = 0;

    const float LEFT_JOYSTICK_X_DEFAULT = 128;
    float LEFT_JOYSTICK_X_RAW = 128;
    float LEFT_JOYSTICK_X_THIS_DEVICE = 128;

    const float LEFT_JOYSTICK_Y_DEFAULT = 128;
    float LEFT_JOYSTICK_Y_RAW = 128;
    float LEFT_JOYSTICK_Y_THIS_DEVICE = 128;
    
    const bool LEFT_JOYSTICK_B_DEFAULT = false;
    bool LEFT_JOYSTICK_B_RAW = false;
    bool LEFT_JOYSTICK_B_THIS_DEVICE = false;
    


    /// Right side controllers
    const int KEYBOARD_RIGHT_DEFAULT = 0;
    int KEYBOARD_RIGHT_RAW = 0;
    int KEYBOARD_RIGHT_THIS_DEVICE = 0;

    const float RIGHT_JOYSTICK_X_DEFAULT = 128;
    float RIGHT_JOYSTICK_X_RAW = 128;
    float RIGHT_JOYSTICK_X_THIS_DEVICE = 128;
    
    const float RIGHT_JOYSTICK_Y_DEFAULT = 128;
    float RIGHT_JOYSTICK_Y_RAW = 128;
    float RIGHT_JOYSTICK_Y_THIS_DEVICE = 128;

    const bool RIGHT_JOYSTICK_B_DEFAULT = false;
    bool RIGHT_JOYSTICK_B_RAW = false;
    bool RIGHT_JOYSTICK_B_THIS_DEVICE = false;


    Servo myservo; 

    int AUTO_REBOOT_COUTNER = 0;
    
    float BATTERY_VOLTAGE_READ = 0;

    int WATCH_DOG_TIMER_MILLIS = 0;

    int SERVO_DELAY_CURRENT_DEGREE = 90;
    int SERVO_DELAY_TIMER_MILLIS = 0;
    int SERVO_TRIMM_DEGREE = 90;

    uint32_t last_trim_update_time; // Timestamp for trim adjustment
    uint32_t trim_update_interval = 500; // Interval for trim adjustment in milliseconds

    // Variables for horn functionality
    uint32_t buzzerPin = 32;               // Pin for the passive buzzer
    uint32_t horn_start_time;        // Time when the horn was activated
    uint32_t last_toggle_time;       // Last time the horn state toggled (for flashing or siren)
    uint32_t last_toggle_time_reverse = 0;       // Last time the horn state toggled (for flashing or siren)
    uint32_t siren_frequency;        // Current frequency for the police siren
    bool siren_direction;            // Direction of frequency change (true = increasing, false = decreasing)
    int horn_mode;          // Current mode of the horn (e.g., off, flash-beep, siren, etc.)
    bool horn_active;       // Flag to check if the horn is active
    bool horn_state = false;        // State of the horn (on or off)
    uint32_t last_toggle_time_heartbeat = 0;
    uint8_t heartbeat_phase = 0;      // Tracks the current phase of the heartbeat pattern
    uint32_t heartbeat_start_time  = 0; // Records the start time of the heartbeat




    // LED variables
    uint8_t ledPin_frontLight = 15;        // Pin for front light LED
    bool led_flash_active = false;           // Whether the LED is flashing
    bool led_on = true;                     // Whether the LED is currently on
    bool led_state = false;                  // Current state of the LED (for flashing)
    uint32_t last_led_toggle_time;   // Last time the LED toggled during flashing


    char G_RADIO_TX[33];
        // [0] - device selector - unique device identifier
        // [1] - Left Joy X
        // [2] - Left Joy Y
        // [3] - Right Joy X
        // [4] - Right Joy Y
        // [5] - 
        // [6] - 
        // [31] - 



    // Persistent system settings struct for non-blocking behavior
    int currentSpeed = 128;     // Current motor speed
    int targetSpeed = 128;      // Desired motor speed (input)
    unsigned long lastUpdate = 0;  // Timestamp of the last speed adjustment

};

#endif

