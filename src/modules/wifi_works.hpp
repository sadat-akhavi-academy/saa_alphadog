#ifndef WIFI_WORKS_HPP
#define WIFI_WORKS_HPP

#include "Arduino.h"
#include "modules/settings_n_structs.hpp"
#include <WiFi.h>

void wifi_works_initi( SYSTEM_SETTINGS_STRUCT *system_settings ) {

    const char* ssid = "";          // Replace with your Wi-Fi network name
    const char* password = "";  // Replace with your Wi-Fi password

    WiFi.begin(ssid, password);

    Serial.println("Connecting to Wi-Fi...");
    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.print(".");
    }

    Serial.println("\nConnected to Wi-Fi!");
    Serial.println("IP Address: ");
    Serial.println(WiFi.localIP());

    
}

void wifi_works_routine( SYSTEM_SETTINGS_STRUCT *system_settings ) {
  
}



#endif

