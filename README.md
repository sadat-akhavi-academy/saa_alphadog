# Sadat Akhavi Academy

Welcome to the AlphaBike repository, brought to you by Sadat Akhavi Academy. This repository contains code for educational toys designed to facilitate learning in a fun and interactive way. 

## About AlphaBike

AlphaBike is a project developed by Sadat Akhavi Academy, a company dedicated to creating innovative educational tools for children. For more information about AlphaBike and our products, please visit our website [www.sadat-akhavi-academy.com](https://www.sadat-akhavi-academy.com).

## Contact Information

For commercial inquiries, partnerships, or any other business-related matters, please contact us at [contactus@sadat-akhavi-academy.com](mailto:contactus@gsadat-akhavi-academy.com).

## Contents

This repository contains the following directories:

- **PlatformIO**: Contains the source code for PlatformIO\VSCode.
- **ArduinoIDE**: Contains the source code for Arduino IDE.
- **docs**: Documentation related to the project.

## Usage

Detailed usage instructions can be found in the documentation provided in the `docs` directory.
